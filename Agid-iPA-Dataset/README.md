# Copie archiviate dei dataset Agid-iPA

L'_Indice dei domicili digitali delle Pubbliche Amministrazioni e dei gestori di pubblici servizi_ (**IPA**), gestito dall'_Agenzia per l'Italia Digitale_ (**AGID**), è l'elenco pubblico di fiducia contenente i domicili digitali da utilizzare per le comunicazioni e per lo scambio di informazioni e per l'invio di documenti validi a tutti gli effetti di legge tra le pubbliche amministrazioni, i gestori di pubblici servizi e i privati di cui all’[Art. 6ter del D.Lgs. 7 marzo 2005, n. 82 (**CAD**)](https://docs.italia.it/italia/piano-triennale-ict/codice-amministrazione-digitale-docs/it/v2018-09-28/_rst/capo1_sezione2_art6-ter.html).

Tale elenco è reso disponibile come open dataset sotto licenza [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode) (_la medesima in uso da questa repository n.d.r._) su https://indicepa.gov.it/ipa-dati/dataset/enti - da cui può anche essere scaricato nei formati CSV/TSV/JSON/XML/XLSX.

\
Dato che il dataset viene aggiornato con frequenza giornaliera, _mantenendo però invariati resource_id / nome dei file_, questa cartella contiene copie archiviate dei dataset utilizzati per le scansioni, onde agevolare verifiche/validazioni terze dei risultati tramite urlscan e/o altri strumenti/metodi.
