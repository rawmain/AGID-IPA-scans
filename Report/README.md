# Report

Rapporti degli inoltri API e di processo/ricerche - contenenti gli URL diretti per la consultazione web (`result`) e/o il recupero JSON (`api`) delle scansioni associate.

| Formato | `result` | `api` |
|---------|----------|-------|
| CSV     | X        | X     |
| JSON    | X        | X     |
| PDF     | X        |       |
