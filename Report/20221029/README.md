## Report AgID-iPA dataset 20221029

- **submission_results** = report di inoltro alle API urlscan per l'avvio del processo di analisi. Disponibili nei formati CSV e JSON

- **processed_ok_results** = report sintetici degli URL processati OK. Disponibili nei formati CSV, JSON (_con gli URL per il recupero diretto via API/Web dei report dettagliati delle singole scansioni_) e PDF (_con gli URL per il recupero diretto via web_).

- **filtered_Pxy_z** = report filtrati per ricerche specifiche (_x = capitolo / y = paragrafo di riferimento / z = ricerca specifica_). Disponibili nei formati CSV, JSON (_con gli URL per il recupero diretto via API/Web dei report dettagliati delle singole scansioni_) e PDF (_con gli URL per il recupero diretto via web_).

- **DOM_scan_e_extra_sub_judice** = riferimenti degli UUID sottoposti a check regexp DOM & degli UUID extra "sub judice" (_per i quali l'esito rimane sospeso, essendo necessario supplemento di verifica navigazione con accettazione manuale o automatizzata di consensi/cookie_).
