# AGID IPA Scans

**_Cronologia :_**
- **_11 Novembre 2022_** - _rilascio report + annotazioni_
- **_31 Ottobre 2022_** - _caricato scan del dataset AgID-iPA aggiornato al 29/10_

\
L'_indice dei domicili digitali delle Pubbliche Amministrazioni e dei gestori di pubblici servizi_ (**iPA**), gestito dall'_Agenzia per l'Italia Digitale_ (**AgID**), è l'elenco pubblico di fiducia contenente i domicili digitali da utilizzare per le comunicazioni e per lo scambio di informazioni e per l'invio di documenti validi a tutti gli effetti di legge tra le pubbliche amministrazioni, i gestori di pubblici servizi e i privati di cui all’[Art. 6ter del D.Lgs. 7 marzo 2005, n. 82 (**CAD**)](https://docs.italia.it/italia/piano-triennale-ict/codice-amministrazione-digitale-docs/it/v2018-09-28/_rst/capo1_sezione2_art6-ter.html).

Tale elenco è reso disponibile come open dataset sotto licenza [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode) (_la medesima in uso da questa repository n.d.r._) su https://indicepa.gov.it/ipa-dati/dataset/enti - da cui può anche essere scaricato nei formati CSV/TSV/JSON/XML/XLSX.

\
Questa repository contiene i riferimenti di analisi effettuate in data 30/10/2022 tramite il servizio [urlscan.io](https://urlscan.io/about/) sugli URL in tale elenco - **versione dataset iPA 29/10/2022**.

Tanto le risultanze quanto le metodologie di tali analisi possono essere riutilizzate nell'ambito di attività terze di assessment/analisi su tecnologie/servizi in uso e/o trasferimenti dati con server di terze parti.

---------------

### Indice

- 1. [**Metodologia usata**](#1-metodologia-usata)

- - 1.1 [Controllo formale URL](#11-controllo-formale-url)

- - 1.2 [Condizioni di scansione](#12-condizioni-di-scansione)

- - 1.3 [Report delle scansioni](#13-report-delle-scansioni)

- - 1.4 [Report filtrati per ricerche specifiche](#14-report-filtrati-per-ricerche-specifiche)

---------------

- 2. [**Rete**](#2-rete)

- - 2.1 [HTTPS](#21-https)

- - 2.2 [AS ed IP](#22-as-ed-ip)

---------------

- 3. [**Statistiche**](#3-statistiche)

- - 3.1 [Web Analytics Italia e Matomo](#31-web-analytics-italia-e-matomo)

- - 3.2 [Google Analytics](#32-google-analytics)

- - - 3.2.1 [GA Client Side](#321-ga-client-side)

- - - 3.2.2 [GA Server Side](#322-ga-server-side)

- - 3.3 [Altri servizi Analytics](#33-altri-servizi-analytics)

----------------

- 4. [**Componenti**](#4-componenti)

- - 4.1 [Bootstrap Italia](#41-bootstrap-italia)

- - 4.2 [Hosted Libraries](#42-hosted-libraries)

- - 4.3 [Font](#43-font)

- - 4.4 [Mappe](#44-mappe)

- - 4.5 [Video](#45-video)

- - 4.6 [CMP](#46-cmp)

- - 4.7 [Captcha](#47-captcha)

-----------------

- 5. [**Considerazioni**](#5-considerazioni)

-----------------
-----------------
-----------------

## 1. Metodologia usata

### 1.1 Controllo formale URL

_**Nota :** Nella cartella [Agid-iPA-Dataset](Agid-iPA-Dataset) sono archiviate le copie delle versioni dei dataset utilizzati._

| versione dataset iPA | Enti/Società | URL univoci in elenco (*) |
|----------------------|--------------|---------------------------|
| 29/10/2022           | 22908        | 21851                     |

> _(*) a seguito di correzione typo & rimozione URL duplicati_

----------------
----------------

### 1.2 Condizioni di scansione

Le scansioni sono state gestite tramite 5 API worker free-tier - attivi in parallelo & tarati con i seguenti vincoli per l'inoltro degli URL :

- delay base di 8s per url-submission singolo worker
- delay base di 300s da url-submission OK per recupero risultato

| URL               | Worker-1 | Worker-2 | Worker-3 | Worker-4 | Worker-5 | **TOTALI** |
|-------------------|----------|----------|----------|----------|----------|------------|
| Inoltrati         | 4500     | 4500     | 4500     | 4500     | 3851     | **21851**  |
| Accettati **(!)** | 4154     | 4459     | 4116     | 3799     | 3519     | **20047**  |

> _**(!)** Respinti 1783 URL causa errore NXDOMAIN (KO risoluzione DNS) + 21 URL incompatibili con l'impostazione `visibility:public` per i risultati delle relative scansioni_

\
**Gli elenchi degli URL inoltrati sono disponibili nella cartella [Elenchi_URL](Elenchi_URL)**.

![alt text](Images/12_elenchi_url.png "Elenchi degli URL inoltrati alle API urlscan")

----------------
----------------

### 1.3 Report delle scansioni

Il processo degli URL accettati è stato effettuato con le seguenti opzioni di configurazione worker :

- `visibility:public` = risultati pubblici & consultabili tramite urlscan senza vincolo di account

- `scanner.country:it` = worker client con indirizzo IP italiano di uscita

- `useragent:default` = User Agent impostato come ultima versione stabile di Google Chrome per Windows 10

- `upgrade-insecure-requests:1` = verifica upgrade HTTP->HTTPS

| URL               | Worker-1 | Worker-2 | Worker-3 | Worker-4 | Worker-5 | **TOTALI** |
|-------------------|----------|----------|----------|----------|----------|------------|
| Accettati         | 4154     | 4459     | 4116     | 3799     | 3519     | **20047**  |
| Processati OK     | 4083     | 4422     | 4071     | 3769     | 3473     | **19818**  |
|                   |          |          |          |          |          |            |
| Processati KO     | 71       | 37       | 45       | 30       | 46       | **229**    |

\
Per 229 URL accettati urlscan ha restituito un risultato di Connection Failure - KO raggiungibilità indirizzo IP associato.

Si è quindi proceduto a re-check supplementare di tali URL, variando solo la modalità di verifica HTTP/HTTPS per l'invio delle request.

|  URL          | Workers | Re-Checked | **TOTALI** |
|---------------|---------|------------|------------|
| Processati OK | 19818   | 101        | **19919**  |

\
**I rapporti di inoltro (prefisso _submission_) e di processo (prefisso _processed_) delle scansioni sono disponibili nella cartella [Report](Report)**.

I rapporti sono disponibili in più formati & contengono gli URL diretti per la consultazione web (`result`) e/o il recupero JSON (`api`) dei risultati dettagliati delle singole scansioni.

| Formato | `result` | `api` |
|---------|----------|-------|
| CSV     | X        | X     |
| JSON    | X        | X     |
| PDF     | X        |       |

--------------------

\
In virtù dell'impostazione `visibility:public` i risultati dettagliati delle singole scansioni sono disponibili pubblicamente su urlscan & accessibili da chiunque, anche senza necessità di account/autenticazione.

Possono essere quindi recuperati tramite :

1. [Ricerca web UI](https://urlscan.io/search/) / [Search API](https://urlscan.io/docs/api/#search), impostando le query ([sintassi ElasticSearch](https://urlscan.io/docs/search/)) con i riferimenti dei `task.tags` associati

2. [Result API](https://urlscan.io/docs/api/#result), indicando direttamente i riferimenti identificativi (UUID) dei risultati di scansione 

\
Si riportano i `task.tags` utilizzati per contrassegnare le attività dataset AND batch dei singoli worker associati, unitamente ai link delle ricerche web con i risultati per i rispettivi processi OK.


| TAG Dataset          |
|----------------------|
| `AGID-IPA-R20221029` |

**AND**

| TAG Worker-1    | TAG Worker-2    | TAG Worker-3     | TAG Worker-4     | TAG Worker-5     | TAG Re-Checked          |
|-----------------|-----------------|------------------|------------------|------------------|-------------------------|
|`RAW-checks-4500`|`RAW-checks-9000`|`RAW-checks-13500`|`RAW-checks-18000`|`RAW-checks-22500`|`RAW-checks-nodata-https`|
| [Risultati Worker-1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20%20(task.tags%3ARAW-checks-4500)%20AND%20(NOT%20stats.uniqIPs%3A0)) | [Risultati Worker-2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20%20(task.tags%3ARAW-checks-9000)%20AND%20(NOT%20stats.uniqIPs%3A0)) | [Risultati Worker-3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20%20(task.tags%3ARAW-checks-13500)%20AND%20(NOT%20stats.uniqIPs%3A0)) | [Risultati Worker-4](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20%20(task.tags%3ARAW-checks-18000)%20AND%20(NOT%20stats.uniqIPs%3A0)) | [Risultati Worker-5](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20%20(task.tags%3ARAW-checks-22500)%20AND%20(NOT%20stats.uniqIPs%3A0)) | [Risultati Re-Checked](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20%20(task.tags%3ARAW-checks-nodata-https)%20AND%20(NOT%20stats.uniqIPs%3A0)) |

> _**Nota :** i processi KO sono stati scartati a livello di query, includendo il parametro `NOT stats.uniqIPs:0`_

------------------
------------------

### 1.4 Report filtrati per ricerche specifiche

Urlscan consente di filtrare ulteriormente i risultati delle scansioni effettuate, arricchendo le query UI/API con parametri di controllo sulle Data Transaction / Client Request, che siano state **effettivamente** registrate dalle scansioni dei worker.

\
Non vi sono infatti opzioni per gestire automaticamente i cookie/consent-wall - _correttamente configurati in conformità al GDPR_ - che blocchino completamente richiamo & esecuzione degli script NON tecnici/necessari (_e quindi anche delle request associate_) qualora/finché non vi sia l'autorizzazione (manuale) del visitatore.

Pertanto, in presenza di wall conforme GDPR, le query dirette/specifiche sul traffico dati non possono individuare un servizio bloccato. Il worker-client non avrà rilevato infatti alcun trasferimento correlabile all'uso di tale servizio... oppure tutt'al più avrà registrato una failed request (_ved. URL rewrite di alcuni cookie/consent-wall_).

-----------------

\
Per questo motivo, nelle ricerche per servizi non tecnici/necessari & rientranti sotto cookie-consent/CMP, si è proceduto anche con le seguenti verifiche ulteriori :

1. **query extra sul traffico**, onde individuare eventuali URL override da parte di cookie/consent-wall 

2. **detected technologies in object `meta.processors.wappa.data`** = urlscan integra tra i suoi moduli anche un processor d'ispezione codice pagina web su base [Wappalyzer](https://github.com/wappalyzer/wappalyzer), che - _seppur non sia pienamente allineato/aggiornato con i pattern ufficiali correnti_ - restituisce comunque info di individuazione tecnologie/servizi nei dettagli dei risultati

3. **regex su `DOM snapshot`** = urlscan salva il Document Object Model (DOM) tree completo dei siti web scansionati, per cui è possibile ricercare (tramite espressioni regolari) riferimenti/pattern nel codice usato dalla pagina web durante la visita del worker-client.

--------------

> _**Nota :**_
>
> _**in caso di URL extra individuati da regex su DOM, questi saranno comunque considerati come "sub judice"** e non saranno sommati (ma solo indicati) in abbinamento ai risultati delle query sul traffico._
> 
> _**Servirebbe infatti comunque un controllo supplementare** (non previsto/effettuato a questo giro n.d.r.) **tramite navigazione con accettazione cookie/CMP** (manuale o automatizzata, e.g. tramite abbinamento di [consent-o-matic](https://github.com/cavi-au/Consent-O-Matic) ad [EDPS WEC](https://github.com/EU-EDPS/website-evidence-collector)), **onde appurare se - accettando tutto - gli script venissero effettivamente richiamati/attivati (o meno) nei siti rilevati solo da DOM regex.**_

---------------

\
Nei paragrafi successivi sono riportati alcuni esempi di ricerche specifiche per determinati servizi in uso sugli URL processati.

Le individuazioni risultanti includono non solo casi d'uso diretto, ma anche indiretto (_richiamo da parte di componenti/servizi di terze parti usati direttamente dai siti_).

\
I parametri di filtro usati per tali ricerche sono utilizzabili via web/API tanto da account free-tier quanto senza autenticazione - cfr. documentazione [Search API](https://urlscan.io/docs/search/) di urlscan - e sono stati abbinati alla seguente query base per estrazione URL processati OK.

|`task.tags:AGID-IPA-R20221029` AND `task.visibility:public` AND `NOT task.tags:RAW-checks-nodata-http` AND `NOT stats.uniqIPs:0`|
|---------|

\
**Le ricerche specifiche con campi/parametri non riportati nei rapporti base di processo (prefisso _processed_) sono presenti in copia nella cartella [Report](Report) (rapporti con prefisso _filtered_)**.

------------------
------------------
------------------

## 2. Rete

### 2.1 HTTPS

Come previsto dal Piano Triennale per l’informatica nella Pubblica Amministrazione, periodicamente AgID effettua rilevazioni sullo stato di sicurezza nei sistemi indicizzati iPA, rilasciando annualmente rapporti di monitoraggio.

Vedansi quelli pubblicati sul sito CERT-AGID - [12/2020](https://cert-agid.gov.it/news/monitoraggio-sul-corretto-utilizzo-del-protocollo-https-e-dei-livelli-di-aggiornamento-delle-versioni-dei-cms-nei-portali-istituzionali-della-pa/) e [10/2021](https://cert-agid.gov.it/news/secondo-monitoraggio-dello-stato-di-aggiornamento-del-protocollo-https-e-dei-cms-sui-sistemi-della-pa/) - relativi all'utilizzo del protocollo HTTPS ed all’aggiornamento dei CMS utilizzati.

\
Nell'attesa del prossimo rapporto AgID sono stati comunque individuati **4936 siti con criticità di vario genere per l'uso di HTTPS**, verificando in particolare le seguenti situazioni :

- KO upgrade HTTP->HTTPS (_NO SSL/TLS - Malconfigurazioni server - Certificati NON validi_)

- Redirect HTTPS->HTTP (_Malconfigurazioni server - Certificati NON validi_)

| Filtro query **HTTPS**     | Descrizione        | Match          |
|----------------------------|--------------------|----------------|
| `NOT page.tlsValidDays:>0` | **Problemi HTTPS** | [**4936**](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(NOT%20page.tlsValidDays%3A%3E0)) |

------------------

\
Sono stati individuati **62 Issuer** di certificati. Questa è la classifica di quelli con associati almeno 100 certificati emessi. 

| Issuer	                                     | Siti	|
|------------------------------------------------|------|
| R3	                                         | 8005	|
| Actalis Domain Validation Server CA G3         | 2799	|
| Sectigo RSA Domain Validation Secure Server CA | 1439	|
| Gandi Standard SSL CA 2	                     | 549	|
| GTS CA 1D4	                                 | 389	|
| cPanel, Inc. Certification Authority	         | 278	|
| GeoTrust RSA CA 2018	                         | 167	|
| DigiCert TLS RSA SHA256 2020 CA1	             | 153	|
| Actalis Organization Validated Server CA G3	 | 133	|
| Cloudflare Inc ECC CA-3	                     | 107	|
| ZeroSSL RSA Domain Secure Site CA	             | 101	|

------------------

------------------

### 2.2 AS ed IP

Gli indirizzi IP IPv4/IPv6 dei 19919 URL univoci processati sono ospitati complessivamente su 324 sistemi autonomi (AS).

22 provider/LIR con 24 AS (_Aruba e Telecom Italia presenti con 2 AS in classifica_) ospitano ciascuno almeno 100 siti. 

| Filtro query AS            | Provider/LIR   | Match          |
|----------------------------|----------------|----------------|
| `page.asnname:aruba*`      | Aruba          | [6079](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aaruba*))               |
| `page.asnname:amazon*`     | Amazon         | [2300](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aamazon*))               |
|`page.asn:AS20746` OR `page.asn:AS3269`| Telecom Italia | [1444](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((page.asn%3AAS20746)%20OR%20(page.asn%3AAS3269)))               |
| `page.asnname:irideos*`    | Irideos        | [1378](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Airideos*))               |
| `page.asnname:genesys*` | Hosting Solutions | [833](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Agenesys*))               |
| `page.asnname:google*`     | Google         | [662](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Agoogle*))               |
| `page.asnname:seeweb*`     | Seeweb         | [590](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aseeweb*))              |
| `page.asnname:fastweb*`    | Fastweb        | [586](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Afastweb*))               |
| `page.asnname:ovh*`        | OVH            | [583](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aovh*))               |
| `page.asnname:serverplan*` | Serverplan     | [583](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aserverplan*))              |
| `page.asnname:sitek*`      | Si.TEK         | [524](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Asitek*))              |
| `page.asnname:microsoft*`  | Microsoft      | [255](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Amicrosoft*))               |
| `page.asnname:hetzner*`    | Hetzner        | [249](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Ahetzner*))               |
| `page.asnname:coltengine*` | Host           | [223](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Acoltengine*))               |
| `page.asnname:register*`   | Register       | [212](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aregister*))               |
| `page.asnname:insiel*`     | Insiel         | [209](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Ainsiel*))               |
| `page.asnname:cloudflare*` | Cloudflare     | [188](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Acloudflare*))               |
| `page.asnname:assupernova*`| Netsons        | [186](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aassupernova*))               |
| `page.asn:AS41651`         | Regione Veneto | [154](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asn%3AAS41651))
| `page.asnname:lepida*`     | Lepida         | [133](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Alepida*))               |
| `page.asnname:itnet*`      | ITnet          | [115](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Aitnet*))               |
| `page.asnname:linode*`     | Linode         | [103](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Alinode*))               |

\
**Poco più del 7.4% degli AS ospita quindi 17589 siti - 88.3% del totale processato**. Il restante 11.7% è _sparpagliato_ sugli altri 300 AS individuati.

Confermato quindi il trend del biennio precedente - anche per quanto riguarda il _podio_ dei fornitori. Vedasi in merito il confronto con i dati del [report AS/IP AgID-iPA - pubblicato 10/2020 da **Antonio Prado**](https://www.prado.it/2020/10/29/siti-web-della-pubblica-amministrazione-italiana-il-report-2020/).

------------------

\
Rilevati **421 server web** distinti per prodotto/versione. Questa è la classifica di quelli individuati in associazione ad almeno 100 siti.

| Server Web                                           | Siti	|
|------------------------------------------------------|--------|
| Apache (undisclosed)	                               | 4777	|
| nginx (undisclosed)	                               | 3961	|
| aruba-proxy	                                       | 2857	|
| Microsoft-IIS/8.5	                                   | 1320	|
| Microsoft-IIS/10.0	                               | 1058	|
| nginx/1.22.1	                                       | 766	|
| Apache/2.4.54	                                       | 480	|
| Apache/2.4.23 (Unix) OpenSSL/1.0.2k PHP/5.6.31	   | 431	|
| Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/5.6.40 | 259	|
| Cloudflare	                                       | 188	|
| Apache/2.2.15 (CentOS)	                           | 171	|
| Apache/2.4.29 (Ubuntu)	                           | 114	|
| nginx/1.14.1	                                       | 100	|


------------------

\
Urlscan consente anche di effettuare ricerche per geolocalizzazione degli IP contattati (_sulla base del controllo incrociato tra IP Whois - AS Whois/Routing - DB Geolocation provider_).

**Geolocalizzazione IP da prendere sempre/comunque con le pinze**, visti i casi in cui servono controlli supplementari/alternativi, onde individuare in modo corretto l'ubicazione geografica dei server contattati (_e.g. IP in global CDN_).

Inoltre, una pur corretta geolocalizzazione IP - _tanto della primary page request quanto delle subrequest_ - non è certo esauriente, qualora si intenda verificare p.es. la [conformità al GDPR dei trasferimenti dati all'estero](https://www.garanteprivacy.it/temi/trasferimento-di-dati-all-estero).  

Tuttavia, può tornare comunque utile per eventuali approfondimenti in incrocio p.es. con i dati relativi agli AS. 

\
**[3879](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(NOT%20((page.ip%3A217.27.72.228)%20OR%20(page.ip%3A46.252.150.197)))%20AND%20(NOT%20page.country%3AIT)%20) siti (19.5% di quelli processati) sono stati individuati con risoluzione DNS su IP _esteri_** - geolocalizzati da urlscan come in relazione a 25 stati/territori stranieri.

| Filtro query GeoIP sito    | Nazione        | Match            |
|----------------------------|----------------|------------------|
| `page.country:IE`          | Irlanda        | [1328](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AIE))             |
| `page.country:US`          | Stati Uniti    | [854](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AUS))             |
| `page.country:DE`          | Germania       | [608](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ADE))             |
| `page.country:FR`          | Francia        | [578](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AFR))             |
| `page.country:NL`          | Olanda         | [322](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ANL))             |
| `page.country:GB`          | Gran Bretagna  | [86](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AGB))             |
| `page.country:CH`          | Svizzera       | [17](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ACH))             |
| `page.country:FI`          | Finlandia      | [16](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AFI))             |
| `page.country:IR`          | Iran           | [12](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AIR))             |
| `page.country:BE`          | Belgio         | [10](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ABE))             |
| `page.country:SM`          | San Marino     | [9](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ASM))             |
| `page.country:ES`          | Spagna         | [7](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AES))             |
| `page.country:AT`          | Austria        | [5](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AAT))             |
| `page.country:DK`          | Danimarca      | [5](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ADK))             |
| `page.country:CA`          | Canada         | [3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ACA))             |
| `page.country:CY`          | Cipro          | [3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ACY))             |
| `page.country:LT`          | Lituania       | [3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ALT))             |
| `page.country:SE`          | Svezia         | [3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ASE))             |
| `page.country:HK`          | Hong Kong      | [2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AHK))             |
| `page.country:KW`          | Kuwait         | [2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3AKW))             |
| `page.country:PT`          | Portogallo     | [2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3APT))             |
| `page.country:BG`          | Bulgaria       | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ABG))             |
| `page.country:PL`          | Polonia        | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3APL))             |
| `page.country:SG`          | Singapore      | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ASG))             |
| `page.country:SI`          | Slovenia       | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.country%3ASI))             |

\
Infine, per quanto riguarda il supporto dual-stack IPv4/IPv6, la relativa situazione non è variata significativamente rispetto al sopracitato report 10/2020 di Antonio Prado.

**Solo 441 siti (2.2% del totale processato) supportano pienamente connessione & accesso su IPv6**. Gli altri siti o sono raggiungibili solo su IPv4 oppure accettano anche la connessione, ma poi switchano comunque la navigazione su IPv4. 

| Filtro query IPv6 sito | Descrizione                   | Match          |
|------------------------|-------------------------------|----------------|
| `page.ip:2000\:\:\/3`  | Check DNS/connessione su IPv6 | [441](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.ip%3A2000%5C%3A%5C%3A%5C%2F3))           |

------------------

------------------

## 3. Statistiche

### 3.1 Web Analytics Italia e Matomo

[**Web Analytics Italia (WAI)**](https://webanalytics.italia.it/) è una piattaforma nazionale - _in fase sperimentale sotto la gestione di AgID_ - deputata a raccolta + analisi dei dati statistici relativi al traffico di siti / servizi digitali della Pubblica Amministrazione.

Il suo sistema di raccolta + analisi è basato su software open source [Matomo](https://github.com/matomo-org/matomo).

\
A fine settembre 2022 risultavano registrate a WAI 2397 PA, per un totale di 3578 siti web attivi ([rif. comunicazione AgID sul potenziamento 01/2023](https://webanalytics.italia.it/#comunicazione-potenziamento)).

Un numero ben superiore ai target previsti di adesione / utenze attive (_700 PA nel 2022 - 850 PA nel 2023_) per i quali era stato dimensionato/tarato il sistema.

----------------------

\
Attualmente, in attesa appunto del potenziamento 01/2023 :

- è stata sospesa la possibilità di effettuare nuove adesioni alla piattaforma da parte delle PA, nonché quella di aggiungere nuovi siti da parte delle PA già registrate

- la dashboard WAI non è pienamente operativa, ma rimane comunque attivo il sistema di raccolta & conservazione dei dati, affinché questi possano essere fruibili dalle PA già registrate, una volta potenziata l'infrastruttura

- le PA registrate possono quindi mantenere attivi gli script di tracciamento WAI, ma transitoriamente devono comunque utilizzare altre soluzioni per elaborare/analizzare nel frattempo le analytics dei loro siti

----------------------

\
Le request associate a WAI & registrate dai worker sono individuabili agevolmente, concatenando alla query base l'AND per il parametro `domain:` valorizzato sul dominio WAI.

| Filtro query **WAI**          | Descrizione                   | Match          |
|-------------------------------|-------------------------------|----------------|
|`domain:webanalytics.italia.it`| Contatto Web Analytics Italia | [1744](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Awebanalytics.italia.it)%20AND%20(NOT%20page.domain%3Awebanalytics.italia.it)))               |

\
WAI non è ancora incluso tra i pattern ricercabili da Wappalyzer. Il controllo sugli DOM snapshot è stato effettuato con una regexp per l'endpoint specificato nel codice/script di tracciamento WAI.

| Regexp                                | DOM Match |
|---------------------------------------|-----------|
| `ingestion\.webanalytics\.italia\.it` | 1853      |

\
1723 URL individuati dalle query sul traffico risultano anche nei risultati DOM, per cui _avanzano 130 URL "sub judice"_.

| **Web Analytics Italia (WAI)** | [**1744**](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Awebanalytics.italia.it)%20AND%20(NOT%20page.domain%3Awebanalytics.italia.it))) **+** _**130**_ |
|--------------------------------|-----------------------------------------------------------------------|

-------------------

\
Complessivamente, le request associate all'uso di soluzioni Matomo su hosting 3rd-party (non solo WAI) o self/on-premise possono essere individuate tramite i seguenti pattern base di filtro.

| Filtro query                                    | Descrizione               | Match |
|-------------------------------------------------|---------------------------|-------|
|`domain:matomo.*`                                |Matomo DOM/Sub             | [90](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Amatomo.*)%20AND%20(NOT%20page.domain%3Amatomo.*)))       |
|`domain:matomo.cloud`                            |Matomo Cloud/Wordpress     | [22](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Amatomo.cloud)%20AND%20(NOT%20page.domain%3Amatomo.cloud)))      |
|`filename:"/matomo.js"`                          |Matomo JS                  | [2072](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fmatomo.js%22))      |
|`filename:"/matomo.php"`                         |Matomo PHP                 | [255](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fmatomo.php%22))      |
|`filename:"/piwik.js"` AND `NOT domain:piwik.pro`|Piwik JS NOT PiwikPRO (!)  | [194](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fpiwik.js%22)%20AND%20(NOT%20((domain%3Apiwik.pro)%20AND%20(NOT%20page.domain%3Apiwik.pro))))      |
|`filename:"/piwik.php"` AND `NOT domain:piwik.pro`|PIwik PHP NOT PiwikPRO (!)| [252](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fpiwik.php%22)%20AND%20(NOT%20((domain%3Apiwik.pro)%20AND%20(NOT%20page.domain%3Apiwik.pro))))      |

> _**(!)** Matomo e [Piwik PRO](https://piwik.pro/) hanno radici comuni, visto che derivano entrambi dal medesimo progetto originario open source Piwik. Tuttavia, dal 2016 sono comunque prodotti/servizi distinti e con differenze marcate per quanto riguarda codice/sviluppo e proposte commerciali._  

-------------------

\
Nel caso di questa ricerca specifica non servono query extra per individuazione di override, vista l'impostazione scelta per i pattern base di controllo del traffico effettuato.

Il processor wappalyzer di urlscan usa il [pattern corrente/ufficiale d'ispezione Matomo Analytics](https://github.com/wappalyzer/wappalyzer/blob/master/src/technologies/m.json#L1007-L1007) `piwik\.js|piwik\.php` per i controlli nel codice delle pagine web.

Pattern per niente esaustivo, visto che rileva infatti solo 336 corrispondenze - tutte peraltro già individuate dalle query base sul traffico.

\
Per il controllo DOM sono state usate 2 regex. Una per escludere hit relativi a PiwikPro e l'altra per verificare tracce di codice correlabili all'uso di Matomo.

| Regexp                                           | Descrizione                 | DOM Match |
|--------------------------------------------------|-----------------------------|-----------|
| `piwik\.pro/piwik\.(js\|php)`                    | Check x esclusione PiwikPRO | 0         |
| `(matomo\.(cloud\|js\|php))\|(piwik\.(js\|php))` | Check x rilevamento Matomo  | 2539      |

\
2367 URL individuati dalle query sul traffico sono presenti anche nei risultati DOM, per cui _avanzano 172 URL "sub judice"_.

| **Matomo Analytics (WAI/3rd-hosted/on-premise)** | [**2401**](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Amatomo.*)%20OR%20(domain%3Amatomo.cloud)%20OR%20(filename%3A%22%2Fmatomo.js%22)%20OR%20(filename%3A%22%2Fmatomo.php%22)%20OR%20((filename%3A%22%2Fpiwik.js%22)%20AND%20(NOT%20domain%3Apiwik.pro))%20OR%20((filename%3A%22%2Fpiwik.php%22)%20AND%20(NOT%20domain%3Apiwik.pro)))) **+** _**172**_    |
|----------------------------------------------|----------|

![alt text](Images/31_wai_matomo_analytics.png "WAI e Matomo Analytics")

------------------

------------------

### 3.2 Google Analytics

[Google Analytics](https://marketingplatform.google.com/about/analytics/) è il servizio Google di raccolta & analisi statistiche (_non solo di siti web, ma anche di applicazioni_).

Implementabile sui siti web in vari modi, a seconda della versione (UA/GA3 e/o GA4) e delle modalità d'integrazione con altri strumenti/servizi della piattaforma Google e/o di altri vendor.

Di base si possono comunque classificare 2 macro-scenari di configurazione :

- **Client-Side** = indipendentemente dalla modalità di integrazione di codice/script di raccolta dati/eventi, la trasmissione dei relativi payload avviene mediante connessione diretta tra i client ed i measurement-server Google

- **Server-Side** = NON vi è comunicazione diretta tra i client ed i measurement-server Google, ma le comunicazioni vengono intermediate da un server terzo a cui i client si collegano direttamente

------------------

------------------

#### 3.2.1 GA Client Side

Innanzitutto è stato verificato se fosse necessario (o meno) impostare condizioni di vincolo per eventuale uso di Google Tag Manager (GTM) in configurazione debug/preview e/o per trasmissione di collection payload GA3/GA4 a server distinti dai measurement-server Google.

| Query ricerca vincolo                                                    | Descrizione                  | Match        |
|--------------------------------------------------------------------------|------------------------------|--------------|
|`domain:tagmanager.google.com`                                            |Contatto GTM Debug/Preview    | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Atagmanager.google.com))                              |
|`NOT domain:analytics.google.com` AND `NOT domain:google-analytics.com` AND `filename:"tid=UA-"`|Collect UA/GA3 NOT Google|[0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22tid%3DUA-%22)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(NOT%20domain%3Aanalytics.google.com))) |
|`NOT domain:analytics.google.com` AND `NOT domain:google-analytics.com` AND `filename:"tid=G-"` |Collect GA4 NOT Google   |[0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22tid%3DG-%22)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(NOT%20domain%3Aanalytics.google.com))) |

-------------------

\
Data l'assenza di vincoli in merito, è possibile impostare le seguenti **query base GA client-side** - per l'individuazione del traffico correlato a GA tra client e measurement-server Google.

| Filtro query base GA client-side                                         | Descrizione                  | Match        |
|--------------------------------------------------------------------------|------------------------------|--------------|
|`domain:analytics.google.com`                                             |Contatto GA                   | [9](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aanalytics.google.com)%20AND%20(NOT%20page.domain%3Aanalytics.google.com)))             |
|`domain:google-analytics.com`                                             |Contatto GA                   | [1845](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Agoogle-analytics.com)%20AND%20(NOT%20page.domain%3Agoogle-analytics.com)))             |
|`domain:stats.g.doubleclick.net`                                          |Contatto GA (DoubleCLick)     | [211](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Astats.g.doubleclick.net)%20AND%20(NOT%20page.domain%3Astats.g.doubleclick.net)))             |
|`domain:googletagmanager.com` AND `filename:"gtag/js?"` AND `filename:"id=UA-"`|Contatto GTM + Richiamo JS UA | [699](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22)))             |
|`domain:googletagmanager.com` AND `filename:"gtag/js?"` AND `filename:"id=G-"` |Contatto GTM + Richiamo JS GA4| [250](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22)))             |
|`filename:"/collect?v=1"` AND `filename:"cid="` AND `filename:"tid=UA-"`  |Collection Request UA/GA3     | [1210](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22%2Fcollect%3Fv%3D1%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DUA-%22)))             |
|`filename:"/collect?v=2"` AND `filename:"cid="` AND `filename:"tid=G-"`   |Collection Request GA4        | [215](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22%2Fcollect%3Fv%3D2%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DG-%22)))             |

**L'unione (_in OR_) delle query base GA client-side restituisce esito match per [1907](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Aanalytics.google.com)%20AND%20(NOT%20page.domain%3Aanalytics.google.com))%20OR%20((domain%3Agoogle-analytics.com)%20AND%20(NOT%20page.domain%3Agoogle-analytics.com))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D1%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DUA-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D2%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DG-%22))%20OR%20((domain%3Astats.g.doubleclick.net)%20AND%20(NOT%20page.domain%3Astats.g.doubleclick.net)))) URL**.

------------------

\
In base all'assenza di vincoli & all'impostazione dei pattern per le query base, sono state considerate le seguenti **query extra GA** - per l'individuazione di request con parametri GA, ma aventi endpoint distinti da tag/measurement-server Google.

| Filtro query extra GA                                                    | Descrizione                     | Match        |
|--------------------------------------------------------------------------|---------------------------------|--------------|
|`NOT domain:googletagmanager.com` AND `filename:"gtag/js?"` AND `filename:"id=UA-"`| GTM + JS UA NOT Google |[1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22)))          |
|`NOT domain:googletagmanager.com` AND `filename:"gtag/js?"` AND `filename:"id=G-"` | GTM + JS GA4 NOT Google|[2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22)))          |
|`NOT domain:analytics.google.com` AND `NOT domain:google-analytics.com` AND `filename:"ga.js"`        | ga.js NOT Google |[1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22ga.js%22)))         |
|`filename:"gdprlock/analytics.js"` | GDPR lock analytics.js |[1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22gdprlock%2Fanalytics.js%22))         |
|`filename:"gdprlock/ga.js"`        | GDPR lock ga.js        |[1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22gdprlock%2Fga.js%22))         |
|`filename:"gdprlock/gtag/js?"` AND `filename:"id=UA-"`                    | GDPR lock GTM + JS UA            |[1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22)))        |
|`filename:"gdprlock/gtag/js?"` AND `filename:"id=G-"`                     | GDPR lock GTM + JS GA4           |[1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22)))        |

**L'unione (_in OR_) delle sole query extra GA restituisce esito match per [4](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20(filename%3A%22gdprlock%2Fanalytics.js%22)%20OR%20(filename%3A%22gdprlock%2Fga.js%22)%20OR%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22ga.js%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22)))) URL.** 

> _**Pattern extra NON considerati :**_
>
> _1. `NOT domain:analytics.google.com` AND `NOT domain:google-analytics.com` AND `filename:"analytics.js"` restituisce [13 match](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22analytics.js%22))), ma sono 11 falsi positivi + 2 hit individuati anche da altre query_
> 
> _2. `NOT domain:analytics.google.com` AND `NOT domain:google-analytics.com` AND `filename:"urchin.js"` restituisce [0 match](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22urchin.js%22)))_
>
> _3. `filename:"gdprlock/urchin.js"` restituisce [0 match](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22gdprlock%2Furchin.js%22))_

---------------------------

\
Non essendo pienamente allineato/aggiornato, il processor wappalyzer di urlscan usa soltanto il primo dei 2 [pattern correnti/ufficiali d'ispezione Google Analytics](https://github.com/wappalyzer/wappalyzer/blob/master/src/technologies/g.json#L1416-L1417) - `google-analytics\.com/(?:ga|urchin|analytics)\.js` - per i controlli nel codice delle pagine web. 

Il processor di urlscan rileva 1820 corrispondenze - tutte già individuate dalle query base GA client-side + GA extra.

\
Per il controllo DOM è stata quindi usata solo la regex per verificare il richiamo dello script gtag JS per UA/GA4 da GTM.

| Regexp                                           | Descrizione                       | DOM Match |
|--------------------------------------------------|-----------------------------------|-----------|
|`(googletagmanager\.com/gtag/js(.*?)id=(UA-\|G-))`| Check x rilevamento GTM JS UA/GA4 | 1027      |

\
854 URL individuati dalle query sul traffico sono presenti anche nei risultati DOM, per cui _avanzano 173 URL "sub judice"_.

| Google Analytics (client-side) | [**1911**](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Aanalytics.google.com)%20AND%20(NOT%20page.domain%3Aanalytics.google.com))%20OR%20((domain%3Agoogle-analytics.com)%20AND%20(NOT%20page.domain%3Agoogle-analytics.com))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D1%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DUA-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D2%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DG-%22))%20OR%20((domain%3Astats.g.doubleclick.net)%20AND%20(NOT%20page.domain%3Astats.g.doubleclick.net))%20OR%20(((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20(filename%3A%22gdprlock%2Fanalytics.js%22)%20OR%20(filename%3A%22gdprlock%2Fga.js%22)%20OR%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22ga.js%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))))) **+** _**173**_ |
|--------------------------------|-----------------------------------------------------|

![alt text](Images/32_wai_matomo_gaclient.png "WAI - Matomo Analytics - Google Analytics (client-side)")

> _**Nota :** dall'intersezione delle query sul traffico risulta l'uso contestuale di Google Analytics (client-side) con WAI per [**205**](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Aanalytics.google.com)%20AND%20(NOT%20page.domain%3Aanalytics.google.com))%20OR%20((domain%3Agoogle-analytics.com)%20AND%20(NOT%20page.domain%3Agoogle-analytics.com))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D1%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DUA-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D2%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DG-%22))%20OR%20((domain%3Astats.g.doubleclick.net)%20AND%20(NOT%20page.domain%3Astats.g.doubleclick.net))%20OR%20(((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20(filename%3A%22gdprlock%2Fanalytics.js%22)%20OR%20(filename%3A%22gdprlock%2Fga.js%22)%20OR%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22ga.js%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))))%20AND%20((domain%3Awebanalytics.italia.it)%20AND%20(NOT%20page.domain%3Awebanalytics.italia.it))) siti & con Matomo (incluso WAI) per [**233**](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Amatomo.*)%20OR%20(domain%3Amatomo.cloud)%20OR%20(filename%3A%22%2Fmatomo.js%22)%20OR%20(filename%3A%22%2Fmatomo.php%22)%20OR%20((filename%3A%22%2Fpiwik.js%22)%20AND%20(NOT%20domain%3Apiwik.pro))%20OR%20((filename%3A%22%2Fpiwik.php%22)%20AND%20(NOT%20domain%3Apiwik.pro)))%20AND%20(((domain%3Aanalytics.google.com)%20AND%20(NOT%20page.domain%3Aanalytics.google.com))%20OR%20((domain%3Agoogle-analytics.com)%20AND%20(NOT%20page.domain%3Agoogle-analytics.com))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20(((domain%3Agoogletagmanager.com)%20AND%20(NOT%20page.domain%3Agoogletagmanager.com))%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D1%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DUA-%22))%20OR%20((filename%3A%22%2Fcollect%3Fv%3D2%22)%20AND%20(filename%3A%22cid%3D%22)%20AND%20(filename%3A%22tid%3DG-%22))%20OR%20((domain%3Astats.g.doubleclick.net)%20AND%20(NOT%20page.domain%3Astats.g.doubleclick.net))%20OR%20(((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((filename%3A%22gdprlock%2Fgtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))%20OR%20(filename%3A%22gdprlock%2Fanalytics.js%22)%20OR%20(filename%3A%22gdprlock%2Fga.js%22)%20OR%20((NOT%20domain%3Aanalytics.google.com)%20AND%20(NOT%20domain%3Agoogle-analytics.com)%20AND%20(filename%3A%22ga.js%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DUA-%22))%20OR%20((NOT%20domain%3Agoogletagmanager.com)%20AND%20(filename%3A%22gtag%2Fjs%3F%22)%20AND%20(filename%3A%22id%3DG-%22))))) siti._ 

------------------

------------------

#### 3.2.2 GA Server Side

Seppur destinate prevalentemente ad utenze aziendali, le configurazioni GA server-side sono comunque utilizzabili anche da siti web senza finalità business, i cui titolari abbiano interesse/esigenza di :

- _trattamenti di prima parte / pieno controllo dei dati raccolti, gestendo direttamente sia il contenuto dei collection payload che le modalità d'inoltro ai measurement-server Google_

- _coerenza dei dati in elaborazione/integrazione con altri servizi_

- _gestione/rispetto di scelte/consensi dei visitatori solo tramite metodi/cookie di prima parte_ 

\
Tutte le configurazioni possibili GA server-side prevedono - _proprio per loro definizione_ - che non vi sia comunicazione diretta tra i client ed i server Google.

Pertanto la loro ricerca/individuazione dall'analisi del traffico di rete viene effettuata, controllando di base :

1. _subrequest analoghe per impostazione a quelle delle config GA client-side, ma non associate a server della rete di raccolta Google_

2. _subrequest con endpoint e/o parametri usati da soluzioni notorie per server-side tagging/tracking_

\
Gli esiti delle query vincolo/extra nei precedenti controlli config GA client-side non hanno evidenziato corrispondenze con la situazione (1).

Passando invece ai controlli (2), è stato individuato l'uso di alcune soluzioni low-cost/effort notorie.

------------------

- [**Host Analytics**](https://analytics.host.it/)

Soluzione custom di Host - orientata soprattutto all'uso di GA4 con supporto measurement API protocol, ma comunque utilizzabile anche con UA/GA3.

Avviata dal 23 Settembre 2022 come public beta. Tutt'ora carente di opzioni per reale controllo/gestione 1° parte dei dati raccolti = decisamente minimale per funzionalità/opzioni disponibili :

1. **Anonimizzazione totale dell'indirizzo IP client** = il parametro `uip` non viene proprio inoltrato ai server Google. Impostazione di sistema & NON modificabile dal titolare). _Su GA4 non vi è rilevamento/indicazione dell'ubicazione geografica in associazione a hit/eventi, mentre su UA/GA3 viene indicata fissa Torino - in virtù della geolocalizzazione IP del server Host da cui arrivano i payload._

2. **Impostazione automatica profilo NPA per site property** = il parametro `npa=1` viene iniettato nei payload inoltrati, disattivando le opzioni remarketing & la personalizzazione della pubblicità per tutti gli eventi/hit - indipendentemente dalle opzioni di data sharing per account/site GA. Impostazione di sistema non disattivabile via admin dashboard, ma modificabile, agendo sul codice dello script di tracciamento.

\
Questi sono gli attuali pattern base per query sul relativo traffico.

| Filtro query                      | Descrizione                                           | Match |
|-----------------------------------|-------------------------------------------------------|-------|
| `domain:js-analytics.cdn.host.it` | Endpoint Host                                         | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Ajs-analytics.cdn.host.it)%20AND%20(NOT%20page.domain%3Ajs-analytics.cdn.host.it)))      |
| `filename:"/v1/ibriditics"`       | Path dell'endpoint                                    | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fv1%2Fibriditics%22))      |
| `filename:"/ibriditics.min.js"`   | Script di tracciamento                                | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fibriditics.min.js%22))      |      

Dall'unione (_in OR_) di tali query base risulta individuato [1 solo sito](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Ajs-analytics.cdn.host.it)%20AND%20(NOT%20page.domain%3Ajs-analytics.cdn.host.it))%20OR%20(filename%3A%22%2Fibriditics.min.js%22)%20OR%20(filename%3A%22%2Fv1%2Fibriditics%22))).

Il controllo regex DOM con keyword `ibriditics` non ne ha rilevati altri.

------------------

- [**Cloudflare Zaraz**](https://www.cloudflare.com/it-it/products/zaraz/)

Servizio Components Manager - proxy [webCM](https://github.com/cloudflare/webcm) di Cloudflare.

Non è una soluzione specifica/mirata solo per l'uso di Google Analytics server-side, visto che consente di aggregare in una singola client request la raccolta di dati per più servizi, elaborandoli successivamente lato server.

Supporta infatti nativamente l'integrazione di 30 strumenti/servizi in ambito Analytics / Advertising / Marketing Automation, nonché permette l'aggiunta di ulteriori servizi tramite JS Managed Components personalizzati.

\
Per quanto riguarda l'uso con UA/GA3 - GA4, da Giugno 2022 sono state sbloccate per tutti i piani Cloudflare (_free inclusi_) sia le opzioni avanzate di gestione del payload via dashboard server che l'impostazione alternativa/manuale dei parametri delle sessioni client.

Permane invece la possibilità solo per i piani Enterprise con sottoscrizione Data Localization Suite (_Regional Services, Customer Metadata Boundary, Geo Key Manager_) di vincolare tutti i trasferimenti worker/server in EU region. 

\
Questi sono gli attuali pattern base per query sul relativo traffico.

| Filtro query                      | Descrizione                     | Match |
|-----------------------------------|---------------------------------|-------|
| `filename:"/cdn-cgi/zaraz/s.js"`  | Script di tracciamento (auto)   | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fcdn-cgi%2Fzaraz%2Fs.js%22)) |
| `filename:"/cdn-cgi/zaraz/i.js"`  | Script di tracciamento (manual) | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fcdn-cgi%2Fzaraz%2Fi.js%22)) |
| `filename:"/cdn-cgi/zaraz/t"`     | Track Data                      | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fcdn-cgi%2Fzaraz%2Ft%22))    |

Dall'unione (_in OR_) di tali query base risulta (_ovviamente_) individuato [1 solo sito](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22%2Fcdn-cgi%2Fzaraz%2Fs.js%22)%20OR%20(filename%3A%22%2Fcdn-cgi%2Fzaraz%2Fi.js%22)%20OR%20(filename%3A%22%2Fcdn-cgi%2Fzaraz%2Ft%22))).

Il controllo regex DOM con pattern `/cdn-cgi/zaraz/` non ne ha rilevati altri.

Rilevati da query/DOM usi page.domain (_non sub-request su domini distinti_) degli script CMP `/cdn-cgi/challenge-platform/` di Cloudflare, che sono utilizzati anche per la gestione opt-in/opt-out dell'iniezione automatica degli script Zaraz. Tuttavia, non sono comunque sufficienti per rilevare individuazioni "sub judice" di uso Zaraz. 

------------------

- [**Stape**](https://stape.io)

Servizio di hosting container [sGTM](https://developers.google.com/tag-platform/tag-manager/server-side) (server-side Google TAG Manager) _"pronti e via"_ - disponibile in 2 distinte tipologie di fornitura :

- **Global sGTM Hosting** = Stape Inc. (_US_) come fornitore/gestore del servizio con container/server in global CDN e pieno accesso alle API di altri servizi erogati global da Stape Inc. .

- **EU sGTM Hosting** = Stape Europe OÜ (_EE - legal entity disgiunta dall'azienda US_) come fornitore/gestore del servizio con container/server in hosting su rete OVH (FR) e NO API/comunicazioni con servizi erogati global da Stape Inc. .

\
Soluzione, che consente ai titolari un elevato livello di gestione per raccolta ed elaborazione dei dati - direttamente tramite la server dashboard.

Sono infatti disponibili tutte le opzioni/scelte, non solo per selezionare quali dati inoltrare - _e quali no_ - ai server Google, ma soprattutto anche quelle per impostare livelli diversi di pseudonimizzazione/anonimizzazione dei dati trasmessi.

\
Questi sono gli attuali pattern base per query sul relativo traffico.

| Filtro query                                    | Descrizione         | Match |
|-------------------------------------------------|---------------------|-------|
| `domain:eup.stape.io`                           | EU sGTM Hosting     | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aeup.stape.io)%20AND%20(NOT%20page.domain%3Aeup.stape.io))) |
| `domain:stape.io` AND `NOT domain:eup.stape.io` | Global sGTM Hosting | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Astape.io)%20AND%20(NOT%20domain%3Aeup.stape.io))) |

Dall'unione (_in OR_) di tali query - `domain:stape.io` - risulta (_ovviamente_) individuato [1 solo sito](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Astape.io)%20AND%20(NOT%20page.domain%3Astape.io))). 

La query extra per ricerca di GTM NON su server Google - `NOT domain:googletagmanager.com` AND `filename:"id=GTM-"` - restituisce 1 falso positivo = CMP gdprlock plugin usato da sito con configurazione Google Analytics client-side. 

Il controllo regex DOM per pattern `\.stape\.io` non ha rilevato altri URL. 

------------------

------------------

### 3.3 Altri servizi Analytics

Giusto un colpo d'occhio - veloce & basato solo sulle query traffico.

---------------------------

| [Mixpanel](https://mixpanel.com/) | [2 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Aapi.mixpanel.com)%20AND%20(NOT%20page.domain%3Aapi.mixpanel.com))%20OR%20((domain%3Aapi-js.mixpanel.com)%20AND%20(NOT%20page.domain%3Aapi-js.mixpanel.com))%20OR%20((domain%3Acdn.mxpnl.com)%20AND%20(filename%3A%22%2Flibs%2Fmixpanel%22)))) |
|-----------------------------------|-------------------------------|

| Filtro query                                          | Descrizione                   | Match          |
|-------------------------------------------------------|-------------------------------|----------------|
| `domain:api.mixpanel.com`                             | Contatto API Mixpanel         | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aapi.mixpanel.com)%20AND%20(NOT%20page.domain%3Aapi.mixpanel.com)))   |
| `domain:api-js.mixpanel.com`                          | Contatto API Mixpanel         | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aapi-js.mixpanel.com)%20AND%20(NOT%20page.domain%3Aapi-js.mixpanel.com)))          |
| `domain:cdn.mxpnl.com` AND `filename:"/libs/mixpanel"`| Richiamo script JS            | [2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdn.mxpnl.com)%20AND%20(filename%3A%22%2Flibs%2Fmixpanel%22))) |

---------------

| [Linkedin Insight](https://www.linkedin.com/help/lms/answer/a489169) | [5 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Acdn.oribi.io)%20AND%20(NOT%20page.domain%3Acdn.oribi.io))%20OR%20((domain%3Asnap.licdn.com)%20AND%20(NOT%20page.domain%3Asnap.licdn.com)%20AND%20(filename%3A%22li.lms-analytics%22)))) |
|----------------------------------------------------------------------|------------|

| Filtro query                                              | Descrizione                   | Match          |
|-----------------------------------------------------------|-------------------------------|----------------|
| `domain:snap.licdn.com` AND `filename:"li.lms-analytics"` | Richiamo script JS            | [5](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Asnap.licdn.com)%20AND%20(NOT%20page.domain%3Asnap.licdn.com)%20AND%20(filename%3A%22li.lms-analytics%22)))          |
| `domain:cdn.oribi.io`                                     | CDN Oribi                     | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdn.oribi.io)%20AND%20(NOT%20page.domain%3Acdn.oribi.io)))          |

---------------

| [Facebook/Meta Pixel](https://www.facebook.com/business/tools/meta-pixel) | [124 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20(((domain%3Aconnect.facebook.net)%20AND%20(NOT%20page.domain%3Aconnect.facebook.net)%20AND%20(filename%3A%22signals%2Fplugins%2Fidentity.js%22))%20OR%20((domain%3Aconnect.facebook.net)%20AND%20(NOT%20page.domain%3Aconnect.facebook.net)%20AND%20(filename%3A%22signals%2Fconfig%22))%20OR%20((domain%3Aconnect.facebook.net)%20AND%20(NOT%20page.domain%3Aconnect.facebook.net)%20AND%20(filename%3A%22fbevents.js%22))%20OR%20((domain%3Afacebook.com)%20AND%20(NOT%20page.domain%3Afacebook.com)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dfb_page_view%22))%20OR%20((domain%3Afacebook.net)%20AND%20(NOT%20page.domain%3Afacebook.net)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dfb_page_view%22))%20OR%20((domain%3Afacebook.com)%20AND%20(NOT%20page.domain%3Afacebook.com)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dpageview%22)%20AND%20(filename%3A%22fbp%3Dfb.%22))%20OR%20((domain%3Afacebook.net)%20AND%20(NOT%20page.domain%3Afacebook.net)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dpageview%22)%20AND%20(filename%3A%22fbp%3Dfb.%22))))  |
|---------------------------------------------------------------------------|-------|

| Filtro query                                                                                       |Descrizione|Match|
|----------------------------------------------------------------------------------------------------|-----------|-----|
|`domain:connect.facebook.net` AND `filename:"signals/plugins/identity.js"`                          |FB Signals |[14](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Aconnect.facebook.net)%20AND%20(NOT%20page.domain%3Aconnect.facebook.net)%20AND%20(filename%3A%22signals%2Fplugins%2Fidentity.js%22))) |
|`domain:connect.facebook.net` AND `filename:"signals/config"`                                       |FB Signals |[77](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Aconnect.facebook.net)%20AND%20(NOT%20page.domain%3Aconnect.facebook.net)%20AND%20(filename%3A%22signals%2Fconfig%22))) |
|`domain:connect.facebook.net` AND `filename:"fbevents.js"`                                          |Richiamo JS|[79](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Aconnect.facebook.net)%20AND%20(NOT%20page.domain%3Aconnect.facebook.net)%20AND%20(filename%3A%22fbevents.js%22))) |  
|`domain:facebook.com` AND `filename:"tr?id="` AND `filename:"ev=fb_page_view"`                      |PageView   |[46](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Afacebook.com)%20AND%20(NOT%20page.domain%3Afacebook.com)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dfb_page_view%22))) |
|`domain:facebook.net` AND `filename:"tr?id="` AND `filename:"ev=fb_page_view"`                      |PageView   |[46](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Afacebook.net)%20AND%20(NOT%20page.domain%3Afacebook.net)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dfb_page_view%22))) |
|`domain:facebook.com` AND `filename:"tr?id="` AND `filename:"ev=pageview"` AND `filename:"fbp=fb."` |PageView   |[69](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Afacebook.com)%20AND%20(NOT%20page.domain%3Afacebook.com)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dpageview%22)%20AND%20(filename%3A%22fbp%3Dfb.%22))) |
|`domain:facebook.net` AND `filename:"tr?id="` AND `filename:"ev=pageview"` AND `filename:"fbp=fb."` |PageView   |[69](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20%20((domain%3Afacebook.net)%20AND%20(NOT%20page.domain%3Afacebook.net)%20AND%20(filename%3A%22tr%3Fid%3D%22)%20AND%20(filename%3A%22ev%3Dpageview%22)%20AND%20(filename%3A%22fbp%3Dfb.%22))) |

> _**Nota :** Effettuata verifica regexp su DOM con il seguente pattern._
>
> | Regexp                                                                               | DOM Match |
> |--------------------------------------------------------------------------------------|-----------|
> |`(connect\.facebook\.net/(.*?)((fbevents\.js)\|(signals/((config/)\|(plugins/identity.js)))))\|(facebook\.(com\|net)/tr\?id=(.*?)(ev=(fb_page_view)\|(pageview)))`| 107 |
>
> _72 URL della regexp DOM risultano individuati dalle query sul traffico. Pertanto, vi sono :_
>
> - _72 URL individuati da query traffico dove Facebook/Meta Pixel viene richiamato direttamente_
>
> - _72 URL individuati da query traffico dove Facebook/Meta Pixel viene richiamato indirettamente_
>
> - _35 URL extra "sub judice" da regexp DOM = servirebbe supplemento di verifica con accettazione cookie/consensi_

---------------------

| [Cloudflare Insights](https://www.cloudflare.com/it-it/insights/) | [221 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Astatic.cloudflareinsights.com)%20AND%20(filename%3A%22%2Fbeacon.min.js%22))) |
|-------------------------|-----------|

| Filtro query                                                           |Descrizione         | Match   |
|------------------------------------------------------------------------|--------------------|---------|
| `domain:static.cloudflareinsights.com` AND `filename:"/beacon.min.js"` | Beacon CF Insights | [221](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Astatic.cloudflareinsights.com)%20AND%20(filename%3A%22%2Fbeacon.min.js%22))) |

> _**Nota :** In questo caso non servirebbe nemmeno un check regexp DOM, onde riscontrare l'effettivo "peso" dei richiami/usi indiretti... di cui c'è evidenza già solo per il fatto che il risultato sia superiore al numero dei siti "orange-clouded" su Cloudflare ([188](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(page.asnname%3Acloudflare*)))._
>
> _Basta già l'intersezione di tale query traffico con quella per servizi **3Bmeteo** - `domain:3bmeteo.com` - a mostrare come su [203 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Astatic.cloudflareinsights.com)%20AND%20(filename%3A%22%2Fbeacon.min.js%22))%20AND%20(domain%3A3bmeteo.com)) il richiamo del beacon Cloudflare Insights venga avviato da moduli 3Bmeteo._
>
> _Il check regexp DOM con pattern `static\.cloudflareinsights\.com/beacon\.min\.js` restituisce infatti solo 7 corrispondenze - tutte individuate dalle query traffico. Pertanto vi sono :_
>
> - _7 URL individuati da query traffico dove Cloudflare Insights viene richiamato direttamente_
>
> - _214 URL (!) individuati da query traffico dove Cloudflare Insights viene richiamato indirettamente_

---------------------
---------------------
---------------------

## 4. Componenti

### 4.1 Bootstrap Italia

La libreria [Bootstrap Italia](https://italia.github.io/bootstrap-italia/) è il set ufficiale di componenti (_CSS/Javascript/Font/Icone_) a disposizione delle PA per la realizzazione di interfacce web.

Basata sugli strumenti dell'[UI-Kit Designers Italia AgID](https://designers.italia.it/kit/) e sulla libreria Bootstrap, viene aggiornata periodicamente ([_ved. ultimi rilasci in data 8 Novembre_](https://twitter.com/DesignersITA/status/1589995347354554368)) ed è disponibile attualmente in 2 versioni :

- **Bootstrap Italia v2** (_ultima release 2.0.9 su base Bootstrap 5_)

- **Bootstrap Italia v1 Legacy** (_ultima release 1.6.4 su base Bootstrap 4_)

\
In base all'analisi del traffico è possibile individuare quanti siti utilizzino (_in toto o in parte_) suoi componenti.

| Bootstrap Italia | [2376 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((filename%3A%22%2Fbootstrap-italia.min.css%22)%20OR%20(filename%3A%22%2Fbootstrap-italia.bundle.min.js%22)%20OR%20(filename%3A%22%2Fbootstrap-italia.min.js%22)%20OR%20(filename%3A%22%2Fbootstrap-italia%2Fdist%2Ffonts%22)%20OR%20(filename%3A%22%2Fbootstrap-italia%2Fdist%2Fsvg%2F%22))) |
|------------------|------------|

| Filtro query                                          | Descrizione              | Match |
|-------------------------------------------------------|--------------------------|-------|
| `filename:"/bootstrap-italia.min.css"`                | CSS                      | [1925](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fbootstrap-italia.min.css%22))  |   
| `filename:"/bootstrap-italia.bundle.min.js"`          | JS (incl. jQuery/Popper) | [1291](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fbootstrap-italia.bundle.min.js%22))  |
| `filename:"/bootstrap-italia.min.js"`                 | JS                       | [421](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fbootstrap-italia.min.js%22))  |
| `filename:"/bootstrap-italia/dist/fonts"`             | Font                     | [451](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fbootstrap-italia%2Fdist%2Ffonts%22))  |
| `filename:"/bootstrap-italia/dist/svg/"`              | Icone                    | [326](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(filename%3A%22%2Fbootstrap-italia%2Fdist%2Fsvg%22))  |

---------------------
---------------------

### 4.2 Hosted Libraries

Un colpo d'occhio a richiamo/uso di librerie ospitate su CDN repo notorie.

---------------------

| [Google Hosted Libraries](https://developers.google.com/speed/libraries) | [2279 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aajax.googleapis.com)%20AND%20(filename%3A%22%2Fajax%2Flibs%2F%22))) |
|--------------------------------------------------------------------------|------|

| Filtro query                                              | Descrizione              | Match |
|-----------------------------------------------------------|--------------------------|-------|
| `domain:ajax.googleapis.com` AND `filename:"/ajax/libs/"` | Richiamo Lib da Google HL| [2279](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aajax.googleapis.com)%20AND%20(filename%3A%22%2Fajax%2Flibs%2F%22)))  |

---------------------

| [jsDelivr](https://www.jsdelivr.com/) | [1263 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acdn.jsdelivr.net)) |
|---------------------------------------|------|


| Filtro query                                          | Descrizione              | Match |
|-------------------------------------------------------|--------------------------|-------|
| `domain:cdn.jsdelivr.net`                             | Richiamo Lib da jsDelivr | [1263](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acdn.jsdelivr.net)) |

---------------------

| [cdnjs (Cloudflare)](https://cdnjs.cloudflare.com/) | [2395 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdnjs.cloudflare.com)%20AND%20(filename%3A%22%2Fajax%2Flibs%2F%22))) |
|------------------------|------|

| Filtro query                                               | Descrizione                        | Match |
|------------------------------------------------------------|------------------------------------|-------|
| `domain:cdnjs.cloudflare.com` AND `filename:"/ajax/libs/"` | Richiamo Lib da cdnjs (Cloudflare) | [2395](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdnjs.cloudflare.com)%20AND%20(filename%3A%22%2Fajax%2Flibs%2F%22))) |

---------------------

![alt text](Images/42_hosted_libraries_gjc.png "Google Hosted Libraries - jsDelivr - cdnjs")

---------------------
---------------------

### 4.3 Font

Un colpo d'occhio all'uso di API/set di caratteri da servizi font-provider notori. 

---------------------

| [Google Fonts](https://fonts.google.com/) | [6663 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Afonts.googleapis.com)%20AND%20(NOT%20page.domain%3Afonts.googleapis.com))%20OR%20((domain%3Afonts.gstatic.com)%20AND%20(NOT%20page.domain%3Afonts.gstatic.com))%20OR%20(((domain%3Agoogleapis.com)%20AND%20(NOT%20page.domain%3Agoogleapis.com))%20AND%20(filename%3A%22webfonts%2Fv1%2Fwebfonts%3Fkey%3D%22))%20OR%20(((domain%3Athemes.googleusercontent.com)%20AND%20(NOT%20page.domain%3Athemes.googleusercontent.com))%20AND%20(filename%3A%22%2Fstatic%2Ffonts%2F%22))%20OR%20((domain%3Afonts.google.com)%20AND%20(NOT%20page.domain%3Afonts.google.com))%20OR%20(((domain%3Athemes.googleusercontent.com)%20AND%20(NOT%20page.domain%3Athemes.googleusercontent.com))%20AND%20(filename%3A%22%2Ffont%3Fkit%3D%22)))) |
|-------------------------------------------|----------|

| Filtro query                                                          | Descrizione                  | Match        |
|-----------------------------------------------------------------------|------------------------------|--------------|
| `domain:fonts.googleapis.com`                                         |Contatto API Google Fonts     | [5779](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Afonts.googleapis.com)%20AND%20(NOT%20page.domain%3Afonts.googleapis.com)))             |
| `domain:fonts.google.com`                                             |Font Request                  | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Afonts.google.com)%20AND%20(NOT%20page.domain%3Afonts.google.com)))           |
| `domain:fonts.gstatic.com`                                            |Font Request                  | [5881](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Afonts.gstatic.com)%20AND%20(NOT%20page.domain%3Afonts.gstatic.com)))             |
| `domain:googleapis.com` AND `filename:"webfonts/v1/webfonts?key="`    |Recupero elenco dinamico Font | [2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Agoogleapis.com)%20AND%20(NOT%20page.domain%3Agoogleapis.com))%20AND%20(filename%3A%22webfonts%2Fv1%2Fwebfonts%3Fkey%3D%22)))             |
| `domain:themes.googleusercontent.com` AND `filename:"/static/fonts/"` |Font Request                  | [13](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Athemes.googleusercontent.com)%20AND%20(NOT%20page.domain%3Athemes.googleusercontent.com))%20AND%20(filename%3A%22%2Fstatic%2Ffonts%2F%22)))             |
| `domain:themes.googleusercontent.com` AND `filename:"/font?kit="`     |Font Request                  | [2](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Athemes.googleusercontent.com)%20AND%20(NOT%20page.domain%3Athemes.googleusercontent.com))%20AND%20(filename%3A%22%2Ffont%3Fkit%3D%22)))             |

--------------------

| [Font Awesome](https://fontawesome.com/) | [2922 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Afontawesome.com)%20OR%20((domain%3Acdnjs.cloudflare.com)%20AND%20(filename%3A%22%2Fajax%2Flibs%2Ffont-awesome%22)))) |
|------------------------------------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:fontawesome.com`                                               | Font Awesome Free/Pro          | [1705](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Afontawesome.com)) |
| `domain:cdnjs.cloudflare.com` AND `filename:"/ajax/libs/font-awesome"` | Font Awesome (cdnjs)           | [1262](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdnjs.cloudflare.com)%20AND%20(filename%3A%22%2Fajax%2Flibs%2Ffont-awesome%22))) | 

--------------------

![alt text](Images/43_font.png "Google Fonts e Font Awesome")

--------------------
--------------------

### 4.4 Mappe

Un colpo d'occhio ad incorporazione/uso di mappe tramite servizi/strumenti notori.

--------------------

| [Google Maps](https://developers.google.com/maps/documentation) | [3047 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Amaps.google.com)%20OR%20(domain%3Amaps.googleapis.com)%20OR%20(domain%3Amaps.gstatic.com)%20OR%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Fmaps%2Fembed%2Fv1%2Fplace%3F%22)))) |
|-----------------------------------------------------------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:maps.google.com`                                               | Map request                    | [334](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Amaps.google.com))         |
| `domain:maps.googleapis.com`                                           | Contatto API Maps              | [3031](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Amaps.googleapis.com))         |
| `domain:maps.gstatic.com`                                              | Map request                    | [2034](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Amaps.gstatic.com))         |
| `domain:google.com` AND `filename:"maps/embed/v1/place?"`              | Contatto API Embed             | [159](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Fmaps%2Fembed%2Fv1%2Fplace%3F%22)))         |

--------------------

| [OpenStreetMap](https://wiki.openstreetmap.org/wiki/Servers) | [442 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aosm.org)%20OR%20(domain%3Aopenstreetmap.*)%20OR%20(domain%3Atile.thunderforest.com)%20OR%20(domain%3Atileserver.memomaps.de))) |
|-----------------------------------------------------------------------------------------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:osm.org`                                                       | OSM org                        | [81](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aosm.org)) |
| `domain:openstreetmap.org`                                             | OSM org                        | [353](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aopenstreetmap.org))         |
| `domain:openstreetmap.de`                                              | OSM DE                         | [4](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aopenstreetmap.de))         |
| `domain:openstreetmap.fr`                                              | OSM FR                         | [10](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aopenstreetmap.fr))         |
| `domain:openstreetmap.*` AND `NOT domain:openstreetmap.org` AND `NOT domain:openstreetmap.de` AND `NOT domain:openstreetmap.fr`                                                                      |Altri TLD openstreetmap? **(!)**| [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aopenstreetmap.*)%20AND%20(NOT%20domain%3Aopenstreetmap.org)%20AND%20(NOT%20domain%3Aopenstreetmap.de)%20AND%20(NOT%20domain%3Aopenstreetmap.fr))) |
| `domain:osm.*` AND `NOT domain:osm.org`                                | Altri TLD osm? **(!)**         | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aosm.org)) |
| `domain:tile.thunderforest.com`                                        | OSM Layer Cycle/Transport      | [8](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Atile.thunderforest.com)) |
| `domain:tileserver.memomaps.de`                                        | OSM Layer ÖPNVKarte            | [3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Atileserver.memomaps.de)) |

> _**(!)** Giusto due semplici query di controllo/esclusione, onde confermare l'assenza di contatti con tile/manager server di [Local Chapter OSM](https://wiki.openstreetmap.org/wiki/Foundation/Local_Chapters#Established_Local_Chapters) su altri TLD per openstreetmap (e.g. BE, CZ, IE, NL, ORG.PL, RU) e osm (e.g. CH)._

--------------------

![alt text](Images/44_mappe.png "Google Maps e OpenStreetMap")

--------------------
--------------------

### 4.5 Video

Un colpo d'occhio ad incorporazione/uso di video da piattaforme notorie.

--------------------

| [Youtube](https://support.google.com/youtube/answer/171780) | [1070 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aytimg.com)%20OR%20(domain%3Ayoutube.com)%20OR%20(domain%3Ayoutube-nocookie.com))) |
|-------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:youtube.com`                                                   | Contatto Youtube               | [934](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Ayoutube.com))         |
| `domain:youtube-nocookie.com`                                          | Embed no-cookie                | [121](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Ayoutube-nocookie.com))         |
| `domain:ytimg.com`                                                     | Thumbnail                      | [892](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aytimg.com))         |

--------------------

| [Vimeo](https://developer.vimeo.com/player/sdk/embed) | [36 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Avimeo.com)%20AND%20(NOT%20page.domain%3Avimeo.com)) |
|-------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:vimeo.com`                                                     | Contatto Vimeo Player/API      | [36](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Avimeo.com)%20AND%20(NOT%20page.domain%3Avimeo.com))       |

--------------------

![alt text](Images/45_video_yt_vimeo.png "Youtube e Vimeo")

--------------------
--------------------

### 4.6 CMP

Un colpo d'occhio all'integrazione di alcuni fornitori notori di soluzioni _"chiavi in mano"_ per gestione cookie/consensi.

--------------------

| [CookieBot (Usercentrics)](https://www.cookiebot.com/)  | [278 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acookiebot.com)) |
|---------------------------------------------------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:consent.cookiebot.com`                                         | Consent Cookiebot              | [278](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aconsent.cookiebot.com))         |
| `domain:consentcdn.cookiebot.com`                                      | CDN Consent Cookiebot          | [261](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aconsentcdn.cookiebot.com))         |
| `domain:cookiebot.com` AND `NOT domain:consent.cookiebot.com` AND `NOT domain:consentcdn.cookiebot.com` | Query di verifica/esclusione   | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acookiebot.com)%20AND%20(NOT%20domain%3Aconsent.cookiebot.com)%20AND%20(NOT%20domain%3Aconsentcdn.cookiebot.com))

--------------------

| [CookieFirst](https://cookiefirst.com/) | [5 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acookiefirst.com)) |
|-----------------------------------------|------------|

| Filtro query                                                      | Descrizione                  | Match |
|-------------------------------------------------------------------|------------------------------|-------|
| `domain:consent.cookiefirst.com`                                  | Consent CookieFirst          | [5](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aconsent.cookiefirst.com)) |
| `domain:cookiefirst.com` AND `NOT domain:consent.cookiefirst.com` | Query di verifica/esclusione | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acookiefirst.com)%20AND%20(NOT%20domain%3Aconsent.cookiefirst.com)) |

--------------------

| [CookieYes](https://www.cookieyes.com/) | [70 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdn-cookieyes.com)%20OR%20(domain%3Acookieyes.com))) |
|---------------|-----------|

| Filtro query                 | Descrizione            | Match |
|------------------------------|------------------------|-------|
| `domain:cookieyes.com`       | Richiamo Cookieyes     | [69](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acookieyes.com))  |
| `domain:cdn-cookieyes.com`   | Richiamo CDN Cookieyes | [70](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Acdn-cookieyes.com))  |

--------------------

| [Iubenda](https://www.iubenda.com/) | [797 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Aiubenda.com)) |
|-------------------------------------|-----------|

| Filtro query                                                           | Descrizione                    | Match        |
|------------------------------------------------------------------------|--------------------------------|--------------|
| `domain:iubenda.com` AND `filename:"cookie-solution/confs/js"`         | Iubenda Cookie Solution        | [756](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aiubenda.com)%20AND%20(filename%3A%22cookie-solution%2Fconfs%2Fjs%22)))         |
| `domain:cdn.iubenda.com` AND `filename:"cs/iubenda_cs.js"`             | Iubenda Cookie/Consent         | [639](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdn.iubenda.com)%20AND%20(filename%3A%22cs%2Fiubenda_cs.js%22)))         |
| `domain:cdn.iubenda.com` AND `filename:"iubenda.js"`                   | Iubenda JS                     | [199](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Acdn.iubenda.com)%20AND%20(filename%3A%22iubenda.js%22)))         |
| `domain:iubenda.com` AND `NOT filename:"cookie-solution/confs/js"` AND `NOT filename:"cs/iubenda_cs.js"` AND `NOT filename:"iubenda.js"`| Query di verifica/esclusione   | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Aiubenda.com)%20AND%20(NOT%20filename%3A%22iubenda.js%22)%20AND%20(NOT%20filename%3A%22cs%2Fiubenda_cs.js%22)%20AND%20(NOT%20filename%3A%22cookie-solution%2Fconfs%2Fjs%22)))      |

----------------------

![alt text](Images/46_cmp.png "CookieBot - CookieFirst - CookieYes - Iubenda")

--------------------
--------------------

### 4.7 Captcha

Un colpo d'occhio ad integrazione/uso di servizi terzi per i controlli captcha.

--------------------

| [Google ReCAPTCHA](https://developers.google.com/recaptcha/intro) | [1640 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi.js%22))%20OR%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi%2F%22))%20OR%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi2%2F%22))%20OR%20((domain%3Agstatic.com)%20AND%20(filename%3A%22%2Frecaptcha%2Freleases%22)))) |
|-------------------------------------------------------------------|-----------|

| Filtro query                                              | Descrizione                     | Match          |
|-----------------------------------------------------------|---------------------------------|----------------|
| `domain:google.com` AND `filename:"/recaptcha/api.js"`    | Google ReCAPTCHA API JS V2/V3   | [1608](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi.js%22)))    |
| `domain:google.com` AND `filename:"/recaptcha/api/"`      | Google ReCAPTCHA API V1 **(!)** | [1](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi%2F%22)))    |
| `domain:google.com` AND `filename:"/recaptcha/api2/"`     | Google ReCAPTCHA API V2         | [1093](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi2%2F%22)))   |
| `domain:gstatic.com` AND `filename:"/recaptcha/releases"` | Request ReCAPTCHA Release       | [1589](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Agstatic.com)%20AND%20(filename%3A%22%2Frecaptcha%2Freleases%22))) |

> _**(!)** La versione V1 di Google ReCAPTCHA è stata disattivata a **Marzo 2018**... per cui la request di tale sito viene comunque inoltrata & ricevuta dai server Google, ma vi è response 404 Not Found._

Check regexp DOM `(google\.com/recaptcha/api(/|2/|\.js))|(gstatic\.com/recaptcha/releases)` restituisce 1635 corrispondenze, già individuate dalle query di traffico.

--------------------

| [hCaptcha](https://docs.hcaptcha.com/configuration) | [3 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Ahcaptcha.com)) |
|-----------------------------------------------------|-----------|

| Filtro query                                           | Descrizione           | Match        |
|--------------------------------------------------------|-----------------------|--------------|
| `domain:js.hcaptcha.com` AND `filename:"/api.js"`      | API hCaptcha                 | [3](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Ajs.hcaptcha.com)%20AND%20(filename%3A%22%2Fapi.js%22))) |
| `domain:hcaptcha.com` AND `NOT domain:js.hcaptcha.com` | Query di verifica/esclusione | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Ahcaptcha.com)%20AND%20(NOT%20domain%3Ajs.hcaptcha.com))) |

--------------------

| [Cloudflare Turnstile](https://developers.cloudflare.com/turnstile/) | [0 siti](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Achallenges.cloudflare.com)%20AND%20(filename%3A%22%2Fturnstile%2Fv0%2Fapi.js%22))) |
|----------------------------------------------------------------------|------------|

| Filtro query                                                           | Descrizione                             |Match|
|------------------------------------------------------------------------|-----------------------------------------|-----|
|`domain:challenges.cloudflare.com` AND `filename:"/turnstile/v0/api.js"`|Cloudflare Turnstile API JS (client-side)| [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20((domain%3Achallenges.cloudflare.com)%20AND%20(filename%3A%22%2Fturnstile%2Fv0%2Fapi.js%22)))        |
| `domain:challenges.cloudflare.com`                                     | Query di verifica/esclusione            | [0](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(domain%3Achallenges.cloudflare.com))

> _**Nota :** Non solo Turnstile si integra senza problemi con gli altri strumenti/servizi Cloudflare, ma [da quasi 2 mesi è pure disponibile gratuitamente per tutti](https://blog.cloudflare.com/turnstile-private-captcha-alternative/), anche per l'uso su siti non "orange-clouded"._
>
> _Eppure, nonostante la possibilità di effettuare alternativamente le [validazioni in configurazione server-side](https://developers.cloudflare.com/turnstile/get-started/server-side-validation/) (opzione non disponibile in altre soluzioni captcha gratuite) senza effettuare alcun trasferimento di dati personali a Cloudflare, finora non è stato ritenuto un servizio interessante._
>
> _Evidentemente, nemmeno dai titolari di quei [17](https://urlscan.io/search/#(task.tags%3AAGID-IPA-R20221029)%20AND%20(task.visibility%3Apublic)%20AND%20(NOT%20task.tags%3ARAW-checks-nodata-http)%20AND%20(NOT%20stats.uniqIPs%3A0)%20AND%20(((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi.js%22))%20OR%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi%2F%22))%20OR%20((domain%3Agoogle.com)%20AND%20(filename%3A%22%2Frecaptcha%2Fapi2%2F%22))%20OR%20((domain%3Agstatic.com)%20AND%20(filename%3A%22%2Frecaptcha%2Freleases%22)))%20AND%20(page.asnname%3Acloudflare*)) siti "orange-clouded", che al 30/10 usavano ancora Google ReCAPTCHA._   

--------------------

![alt text](Images/47_captcha.png "Google reCAPTCHA - hCaptcha - Cloudflare Turnstile")

--------------------
--------------------
--------------------

## 5. Considerazioni

Lo scopo di queste analisi è quello di fornire intanto uno snapshot sulla situazione degli URL indicizzati AgID-iPA, che possa essere utile tanto per altre analisi terze quanto per verifiche degli stessi amministratori/gestori dei siti web in questione.

In tal senso ho scelto di usare urlscan, proprio perché consente di consultare/vagliare i risultati di scansione in modo esaustivo - anche tramite un'interfaccia web comoda, non solo tramite l'analisi degli output testuali JSON di Search/Result API.

\
L'intento sarebbe peraltro realizzare un nuovo snapshot pubblico a distanza di 3 mesi - _quindi a fine Gennaio 2023_ - in modo da poter verificare la situazione a seguito di :

1. Pubblicazione rapporto monitoraggio 2022 di Cert-AgID sullo stato di sicurezza (_perlomeno su HTTPS e stato di aggiornamento CMS_)

2. Ripristino piena operatività di Web Analytics Italia (WAI) - _preventivata appunto per Gennaio 2023_

3. Test con più strumenti (alternativi o addizionali all'abbinamento consent-o-matic + EDPS WEC), onde consentire anche ai soggetti interessati una consultazione agevole degli esiti di verifica su URL individuati "sub judice"
